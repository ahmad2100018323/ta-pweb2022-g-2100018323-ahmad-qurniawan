<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PROJECT AKHIR</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<nav>
		<div class="wrapper">
			<div class="logo"><a href=''>Olshop.Ahmad</a></div>
            <div class="menu">
               <ul>
                   <li><a href="#login">Login</a></li>
                   <li><a href="#daftar">Daftar Barang</a></li>
                   <li><a href="#beli">Beli</a></li>
                   <li><a href="#contact">Contact</a></li>   
               </ul>
          </div>
		</div>
	</nav>
	<section id="login">
			<div class="wrapper">
            <div class="home">
                <div class="home-section">
                    <div class="login">
                    <form action="#" method="POST" onsubmit="validasi()">
            <div>
                <label>
                    Nama Lengkap:
                </label>
                <input type="text" name="nama" id="nama">
            </div>
            <div>
                <label>
                    Email:
                </label>
                <input type="email" name="email" id="email">
            </div>
            <div>
                <label>
                    Password:
                </label>
                <input type="password" name="password" id="password">
            </div>
            <div>
                <label>
                    Website:
                </label>
                <input type="website" name="website" id="website">
            </div>

            <div>
                <label>
                    Alamat:
                </label>
                <textarea cols="40" rows="5" name="alamat" id="alamat">
                </textarea>
            </div>
            <div>
                <input type="submit" value="Login" class="tombol">
            </div>
        </form>
    </div>
</div>
</div>
</div>
</section>
        <section id="daftar">
            <div class="tengah">
                <div class="kolom">
                    <p class="deskripsi"></p>
                    <h2>Daftar Barang</h2>
                </div>
                <div class="barang-list">
                    <div class="barang">
                        <img src="https://th.bing.com/th/id/OIP.xbykLbsM7cdGS_nM22ftawHaHS?w=219&h=217&c=7&r=0&o=5&dpr=1.25&pid=1.7"/>
                        <p>Barang 1</p>
                    </div>
                    <div class="barang">
                        <img src="https://3.bp.blogspot.com/-R0Pjd6kc9DA/VAnEXmJ3rUI/AAAAAAAAACI/KqJu6Gn1S6o/s1600/jual%2Bcelana%2Bchino%2Bjogger%2Bcustom%2Bmurah%2Bdi%2Bbandung%2B(2).jpg"/>
                        <p>Barang 2</p>
                    </div>
                    <div class="barang">
                        <img src="https://bajufutsal.co.id/wp-content/uploads/2017/12/Gambar-Desain-Baju-Bola-keren.jpg"/>
                        <p>Barang 3</p>
                    </div>
                    <div class="barang">
                        <img src="https://harga-jual.com/wp-content/uploads/2018/02/dh-kaos-kaki-olahraga-black-1.jpg"/>
                        <p>Barang 4</p>
                    </div>
                </div>
            </div>
        </section>
<section id="beli">
<div class="wrapper">
	<div class="beli">
                <div class="beli-section">
                    <div class="login">
                    <form action="pro.php" method="POST" name="fform">
            <div>
                <label>
                    Nama Barang:
                </label>
                <input type="text" name="inamabarang" id="nama">
            </div>
            <div>
                <label>
                    Harga Barang:
                </label>
                <input type="text" name="ihargabarang" id="harga">
            </div>
            <div>
                <label>
                    Jarak:
                </label>
                <input type="text" name="ijarak" id="jarak">
            </div>
             <div>
                <label>
                    Total Bayar:
                </label>
                <input type="text" name="ototal" id="total">
            </div>
            <div>
                <input type="submit" name="submit" value="Hitung" class="tombol" onclick="hitungtotal()">
                <input type="reset" value="Ulang" class="tombol">
            </div>
            <div class="cek" align="center"><a href="out.php">::Cek Data Pengiriman::</a></div>
        </form>
</div>
</div>
</div>
</div>
</section>
 <div id="contact">
        <div class="wrapper">
            <div class="footer">
                <div class="footer-section">
                    <h3>Alamat</h3>
                    <p>Kampung Sawit Permai, Kecamatan Dayun, Kabuapten Siak, Provinsi Riau</p>
                    <p>Kode Pos: 657654</p>
                </div>
                 <div class="footer-section">
                    <h3>Email</h3>
                    <p>ahmadqurniawan@gmail.com</p>
                </div>
                <div class="footer-section">
                    <h3>Telp/Hp</h3>
                    <p>+62 8228 8310 0563</p>
                </div>
            </div>
        </div>
    </div>

    <div id="copyright">
           Copyright &copy; 2022. All Rights Reserved.
    </div>

<script type="text/javascript">
    var tombolMenu = document.getElementsByClassName('tombol-menu')[0];
	var menu = document.getElementsByClassName('menu')[0];
	tombolMenu.onclick = function() {
    menu.classList.toggle('active');
}
	menu.onclick = function() {
    menu.classList.toggle('active');
}

function validasi(){
        var nama = document.getElementById("nama").value;
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        var website = document.getElementById("website").value;
        var alamat = document.getElementById("alamat").value;
        if (nama != "" && email !="" && password !="" && website !="" && alamat !=""){
            alert('Anda berhasil login silahkan melihat daftar barang');
        }else{
            alert('Anda harus mengisi data dengan lengkap !');
        }
    }
   function hitungtotal(){
        var namabarang = (document.fform.inamabarang.value);
        var hargabarang = parseFloat(document.fform.ihargabarang.value);
        var jarak =parseFloat(document.fform.ijarak.value);
        var ongkir= 0.0;
        ongkir = 5000;
        var total =0.0; 
        total = (jarak*ongkir)+hargabarang;
        document.fform.ototal.value=eval(total);
    }
    </script>
</body>
</html>